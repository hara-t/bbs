<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>掲示板ホーム</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<link rel="icon" href="./takagi.ico">

</head>
<script>
	function activeDialog() {
		var txt;
		var rt = confirm("本当に削除しますか？");
		if (rt == true) {
			txt = "「ok」を選択しました。";
			return true;
		} else {
			txt = "「キャンセル」を選択しました。";
			return false;
		}
		document.getElementById("conf3").innerHTML = txt;
	}
	</script>
	<script type="text/javascript" src="
js/jquery-3.4.0.min.js">
	let takagi = document.getElementById("takagi");
	takagi.addEventListener("click",clicked);
	function clicked(){
		alert("わ～た～し～が～お～もう");
	}
</script>
<body>
<button id = "takagi"></button>
	<c:if test="${loginUser.id == 13}">
		<img src="takagi.ico" id="takagi">
	</c:if>
	<c:if test="${ not empty loginUser }">
		<div class="profile">
			<div class="name">
				<h2>
					<c:out value="${loginUser.name}" />
				</h2>
			</div>
		</div>
	</c:if>
	<div class="header">
		<c:if test="${ not empty loginUser }">
			<a href="logout">ログアウト</a>
		</c:if>
	</div>
	<a href="newMessage">新規投稿</a>
	<c:if test="${loginUser.branch == 1 && loginUser.department == 1}">
		<a href="userManage ">ユーザ管理</a>
	</c:if>
	<%--エラメ表示 変数同じにしてどこのエラーもここに入れたい--%>
	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="message">

					<li><c:out value="${message}" />
				</c:forEach>
			</ul>
		</div>
		<c:remove var="errorMessages" scope="session" />
		<!--  セッションに保存したエラメを消す 消さないとセッション中ずっと残る -->
	</c:if>

	<form method="get" action="./">
		<div>
			<h2>絞り込み検索</h2>
		</div>
		<div>
			<h6>カテゴリ検索</h6>
			<label><input type="text" name="search" maxlength="20"
				size="20" c:out value="${category}"> </label>
		</div>
		<h6>日時検索</h6>
		<label><input type="date" name="firstDate" min="${minDate}"
			max="${maxDate}" value="${firstDate}"> <c:if
				test="${firstDate}">checked</c:if></label> <label><input type="date"
			name="lastDate" min="${minDate}" max="${maxDate}" value="${lastDate}">
			<c:if test="${lastDate}">checked</c:if></label> <input type="submit"
			value="検索">
		<div class="form">
			<div class="radio">
				<label> <input type="radio" name="sort" value="1"
					<c:if test="${sort == 1}">checked</c:if>> 新しい順
				</label>
			</div>

			<div class="radio">
				<label> <input type="radio" name="sort" value="0"
					<c:if test="${sort == 0}">checked</c:if>> 古い順
				</label>
			</div>
		</div>
	</form>



	<div class="postdispley">

		<h1>投稿一覧</h1>
		<c:forEach items="${posts}" var="post">
			<c:if test="${post.delete == 0 }">

				<div class="post">
					<fieldset>
						<legend>投稿</legend>
						<div class="name">
							投稿者：
							<c:out value="${post.name}" />
						</div>
						<div class="category">
							カテゴリー：
							<c:out value="${post.category}" />
						</div>
						<div class="title">
							件名：
							<c:out value="${post.title}" />
						</div>
						<div class="body">
							本文：
							<c:out value="${post.body}" />
						</div>

						<div class="date">
							投稿日時：
							<fmt:formatDate value="${post.created_date}" pattern="yyyy/MM/dd HH:mm:ss" />
						</div>
						<div class="postdelete">
							<c:if test="${post.user_id == loginUser.id}">
								<form action="deletePost" method="post">
									<button class="btn" type="submit" name="deleted" value="1"
										onClick="return activeDialog()">投稿削除</button>
									<p id="conf3"></p>
									<input type="hidden" name="autopostid" value="${post.id}">
								</form>
							</c:if>
						</div>
					</fieldset>
				</div>


				<div class="commentinput"></div>
				<div class="commentdispley">
					<br>
					<h5>コメント欄</h5>

					<c:forEach items="${comments}" var="comment">

						<c:if test="${post.id == comment.post_id}">
							<!-- コメントのオートではいるようにしている投稿番号と投稿idを比べる　動作済み -->

							<c:if test="${comment.delete == 0 }">
								<div class="commentall">
									<div class="comment">
										コメント：
										<c:out value="${comment.comment}" />
									</div>
									<div class="category">
										コメント投稿者：
										<c:out value="${comment.name}" />
									</div>
									<div class="date">
										コメント投稿日時：
										<fmt:formatDate value="${comment.created_date}"
											pattern="yyyy/MM/dd HH:mm:ss" />
									</div>
									<div class="commentdelete">
										<c:if test="${comment.user_id == loginUser.id}">
											<form action="deleteComment" method="post">
												<button class="btn" type="submit" name="deleted" value="1"
													onClick="return activeDialog()">コメント削除</button>
												<p id="conf3"></p>
												<input type="hidden" name="autocommentid"
													value="${comment.id}">
											</form>
										</c:if>
									</div>
								</div>
							</c:if>
						</c:if>
					</c:forEach>
				</div>
				<form action="newComment" method="post">
					コメントを投稿する(500文字以下)<br>
					<textarea name="comment" cols="100" rows="5" maxlength="500"></textarea>
					<br /> <input type="submit" value="コメント" /> <br /> <input
						name="autoinsert" value="${post.id}" type="hidden" /><br> <br>
					<br>
				</form>
			</c:if>
		</c:forEach>
	</div>


</body>
</html>