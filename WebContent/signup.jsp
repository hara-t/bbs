<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー登録</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<link rel="icon"  href="./takagi.ico">
</head>
<body>
<h2>ユーザー新規登録</h2>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<form action="signup" method="post">
			<br /> <label for="name">名前</label> <input name="name" id="name" /><br />
			<label for="login_id">ログインid</label> <input name="login_id"
				id="login_id" /> <br /> <label for="password">パスワード</label> <input
				name="password" type="password" id="password" /> <br />
				<label for="passcheck">パスワード確認用</label> <input
				name="passcheck" type="password" id="passcheck" /> <br />

				<select
				name="branch">
				<c:forEach items="${branches}" var="branch">
					<option value="${branch.id}">${branch.name}</option>
				</c:forEach>
			</select><br />
			<select name="department">
				<c:forEach items="${departments}" var="department">
					<option value="${department.id}">${department.name}</option>
				</c:forEach>
			</select><br /> <input type="submit" value="登録" /> <br /> <a href="./">ホームへ戻る</a>
		</form>
		<div class="copyright">Copyright(c)hara takaaki</div>
	</div>
</body>
</html>