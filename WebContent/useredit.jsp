<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー編集画面</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<link rel="icon"  href="./takagi.ico">
</head>
<body>
<h2>ユーザー編集　スペースとかじゃなく場所指定で上下のボックスそろえたい</h2>
	<div class="errorMessage ">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
	</div>
	<form action="userEdit" method="post" autocomplete="off">
		<input name=id value="${editUser.id}" id="id" type="hidden">
		<fieldset>
			<legend>ユーザー情報編集</legend>

			<div>
				<label for="login_id">ログインID(半角英数字6～20文字以下)</label> <input
					class="form-control" type="text" name="login_id"
					placeholder="ログインIDを変更" maxlength="20" size="40" id="login_id"
					value="${editUser.loginId}" />
			</div>

			<div>
				<label for="password">パスワード(半角文字6～20文字以下)</label> <input
					class="form-control" name="password"
					placeholder="変更しない場合は何も入力しないでください" maxlength="20" size="40" type="password"
					id="password" />
			</div>

<div>
			<label for="passcheck">パスワード確認用</label> <input
				name="passcheck" type="password" size="40" id="passcheck" /> <br />
</div>

			<div>
				<label for="name">名前(10文字以下)</label> <input class="form-control"
					name="name" placeholder="名称を入力" maxlength="10" size="40" type="text"
					id="name" value="${editUser.name}" />

			</div>


			<div>
				<label for="branch">支店</label> <select name="branch">
					<c:forEach items="${branches}" var="branch">

						<option value="${branch.id}"
							<c:if test="${ branch.id == editUser.branch }">selected</c:if>>
							<c:out value="${branch.name}" />
						</option>
					</c:forEach>
				</select><br />
			</div>

			<div>
				<label for="department">部署・役職</label> <select name="department">
					<c:forEach items="${departments}" var="department">
						<option value="${department.id}"
							<c:if test="${ department.id == editUser.department }">selected</c:if>>
							<c:out value="${department.name}" />
						</option>
					</c:forEach>
				</select>
			</div>
		</fieldset>

		<input type="submit" value="変更" />
	</form>
	<a href="./">ホームへ戻る</a>   <a href="signup">ユーザー新規登録画面へ</a>   <a href="userManage">ユーザー管理画面へ</a>
</body>
</html>