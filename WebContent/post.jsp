<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>新規投稿画面</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<link rel="icon"  href="./takagi.ico">
</head>
<body>
<h2>新規投稿</h2>
 <c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session"/>
			</c:if>
<form action="newMessage"<%--topに遷移したいけど、サーブレットで指定しないと？ --%> method="post">
<label for ="category">カテゴリー(10文字以下)</label>
 <input type ="text" name="category" size="40" maxlength="10" value="${postinput.category}" ></input><br/>
<label for="title">件名(30文字以下)</label>
                <input type ="text" name="title" size="40" maxlength="30" value="${postinput.title}" ></input>
                <br />
 <label for="body">本文(1000文字以下)</label>
                <textarea name="body" cols="35" rows="5" id="body" maxlength="1000" >${postinput.body}</textarea>
                <br />
                <input type="submit" value="投稿" /> <br />
                </form><a href="./">ホームへ戻る</a>
                <a href="logout">ログアウト</a>
</body>
</html>