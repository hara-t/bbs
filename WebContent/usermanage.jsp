<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー管理画面</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<link rel="icon"  href="./takagi.ico">
</head>
<script>
	function activeDialog() {
		var txt;
		var rt = confirm("本当に停止させますか？");
		if (rt == true) {
			txt = "「ok」を選択しました。";
			return true;
		} else {
			txt = "「キャンセル」を選択しました。";
			return false;
		}
		document.getElementById("conf1").innerHTML = txt;
	}
	function stopDialog() {
		var txt;
		var rt = confirm("本当に復活させますか？");
		if (rt == true) {
			txt = "「ok」を選択しました。";
		} else {
			txt = "「キャンセル」を選択しました。";
		}
		document.getElementById("conf2").innerHTML = txt;
	}
</script>

<body>
<h2>ユーザー管理</h2>
	<a href="./">ホームへ</a>
	<a href="signup">ユーザー新規登録</a>
	<div class="userlist">
		<table border="1">
			<tr>
				<th>名前</th>
				<th>支店</th>
				<th>役職</th>
				<th>編集</th>
				<th>停止</th>
			</tr>
			<c:forEach items="${users}" var="user">
				<tr>
					<td><span class="name"><c:out value="${user.name}" /></span></td>
					<td><span class="branch_name"><c:out
								value="${user.branch_name}" /></span></td>
					<td><span class="department_name"><c:out
								value="${user.department_name}" /></span></td>

					<td><a href="userEdit?id=${user.id}">編集</a></td>
					<td>
						<form action="userManage" method="post">

							<c:if test="${ user.isStopped == 0 }">
								<p>
									<button class="btn" type="submit" name="stop_ornot" value="1"
										onClick="return activeDialog()">停止</button>
								</p>
								<p id="conf1"></p>
								<input type="hidden" name="id" value="${user.id}">
							</c:if>

							<c:if test="${ user.isStopped == 1 }">
								<p>
									<button class="btn" type="submit" name="stop_ornot" value="0"
										onClick="return stopDialog()">復活</button>
								</p>
								<p id="conf2"></p>
								<input type="hidden" name="id" value="${user.id}">
							</c:if>
						</form>
					</td>
			</c:forEach>
		</table>
	</div>

</body>
</html>