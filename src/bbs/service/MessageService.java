package bbs.service;

import static bbs.utils.CloseableUtil.*;
import static bbs.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import bbs.beans.Message;
import bbs.beans.UserPost;
import bbs.dao.MessageDao;
import bbs.dao.UserPostDao;



public class MessageService {

    public void register(Message message) {

        Connection connection = null;
        try {
            connection = getConnection(); //ＤＢへの接続

            MessageDao messageDao = new MessageDao();//インスタンス生成
            messageDao.insert(connection, message);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    private static final int LIMIT_NUM = 1000;

public List<UserPost> getPost() {

    Connection connection = null;
    try {
        connection = getConnection();

        UserPostDao messageDao = new UserPostDao();//インスタンス生成
        List<UserPost> ret = messageDao.getUserPosts(connection, LIMIT_NUM);

        commit(connection);

        return ret;
    } catch (RuntimeException e) {
        rollback(connection);
        throw e;
    } catch (Error e) {
        rollback(connection);
        throw e;
    } finally {
        close(connection);
    }
}


//新しいサーチポスト　全部一気にやりたい
public List<UserPost> searchPost(String category,String firstDate ,String lastDate,int sort) {

    Connection connection = null;
    try {
        connection = getConnection();

        UserPostDao messageDao = new UserPostDao();//インスタンス生成
        List<UserPost> ret = messageDao.searchPost(connection,category,firstDate,lastDate,sort);

        commit(connection);

        return ret;
    } catch (RuntimeException e) {
        rollback(connection);
        throw e;
    } catch (Error e) {
        rollback(connection);
        throw e;
    } finally {
        close(connection);
    }
}


//カテゴリー検索
public List<UserPost> Category(String category) {

 Connection connection = null;
 try {
     connection = getConnection();

     UserPostDao messageDao = new UserPostDao();//インスタンス生成
     List<UserPost> ret = messageDao.Category(connection, LIMIT_NUM,category);

     commit(connection);

     return ret;
 } catch (RuntimeException e) {
     rollback(connection);
     throw e;
 } catch (Error e) {
     rollback(connection);
     throw e;
 } finally {
     close(connection);
 }
}

//投稿を削除する
public void deletePost(UserPost deletepost) {

  Connection connection = null;
  try {
      connection = getConnection();

      UserPostDao.delete(connection, deletepost);

      commit(connection);
  } catch (RuntimeException e){
      rollback(connection);
      throw e;
  } catch (Error e) {
      rollback(connection);
      throw e;
  } finally {
      close(connection);
  }
}

//最古の投稿日時を取得

public String getFirstDate() {

  Connection connection = null;
  try {
      connection = getConnection();

      String firstDate = UserPostDao.getFirstDate(connection);

      commit(connection);

      return firstDate + " 00:00:00";
  } catch (RuntimeException e) {
      rollback(connection);
      throw e;
  } catch (Error e) {
      rollback(connection);
      throw e;
  } finally {
      close(connection);
  }
}

// 最新の投稿日時を取得

public String getLastDate() {

  SimpleDateFormat sdf = new SimpleDateFormat();
  sdf.applyPattern("yyyy-MM-dd");

  Date date = new Date();
  String ret = sdf.format(date);

  return ret + " 23:59:59";



}

}