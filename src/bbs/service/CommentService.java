package bbs.service;

import static bbs.utils.CloseableUtil.*;
import static bbs.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import bbs.beans.Comment;
import bbs.beans.UserComment;
import bbs.beans.UserPost;
import bbs.dao.CommentDao;
import bbs.dao.UserCommentDao;



public class CommentService {

	//コメントの登録処理
    public void register(Comment comment) {

        Connection connection = null;
        try {
            connection = getConnection(); //ＤＢへの接続

            CommentDao commentDao = new CommentDao();
            commentDao.insert(connection, comment);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //コメントの表示処理　結合したdaoを呼ぶ　すべてのコメントと必要な情報を取得する
    private static final int LIMIT_NUM = 500;

public List<UserComment> getComment() {

    Connection connection = null;
    try {
        connection = getConnection();

        UserCommentDao commentDao = new UserCommentDao();
        List<UserComment> ret = commentDao.getUserComments(connection, LIMIT_NUM);

        commit(connection);

        return ret;
    } catch (RuntimeException e) {
        rollback(connection);
        throw e;
    } catch (Error e) {
        rollback(connection);
        throw e;
    } finally {
        close(connection);
    }
}


//コメントを削除する
public void deleteComment(Comment comment) {

    Connection connection = null;
    try {
        connection = getConnection();

        CommentDao.delete(connection, comment);

        commit(connection);
    } catch (RuntimeException e){
        rollback(connection);
        throw e;
    } catch (Error e) {
        rollback(connection);
        throw e;
    } finally {
        close(connection);
    }
}

//特定の投稿に対して、全てのコメントを削除する
public void deletePostComment(UserPost deletepostcomment) {

  Connection connection = null;
  try {
      connection = getConnection();

      CommentDao.deletePostComment(connection, deletepostcomment);

      commit(connection);
  } catch (RuntimeException e){
      rollback(connection);
      throw e;
  } catch (Error e) {
      rollback(connection);
      throw e;
  } finally {
      close(connection);
  }
}

}