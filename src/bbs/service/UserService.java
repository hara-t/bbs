package bbs.service;

import static bbs.utils.CloseableUtil.*;
import static bbs.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import bbs.beans.Branch;
import bbs.beans.Department;
import bbs.beans.User;
import bbs.dao.UserDao;
import bbs.utils.CipherUtil;

public class UserService {

	public void register(User user) {

		Connection connection = null;
		try {
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			UserDao userDao = new UserDao();
			userDao.insert(connection, user);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}


	//ユーザー管理画面の表示のためユーザの名前支店役職を取る
		public List<User> getUsers() {
			Connection connection = null;
			try {
				connection = getConnection();

				UserDao userDao = new UserDao();
				return userDao.getUsers(connection);

			} catch (RuntimeException e) {
				rollback(connection);
				throw e;
			} catch (Error e) {
				rollback(connection);
				throw e;
			} finally {
				close(connection);
			}
		}
//登録のとき表示するために支店リストをリストへ
	public List<Branch> getBranches() {
		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			return userDao.getBranches(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
//編集アプデのため
	 public void update(User user) {

	        Connection connection = null;
	        try {
	            connection = getConnection();

	            String encPassword = CipherUtil.encrypt(user.getPassword());
	            user.setPassword(encPassword);

	            UserDao userDao = new UserDao();
	            userDao.update(connection, user);

	            commit(connection);
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
	    }
	//編集アプデのためぱすなし
		 public void updateNoPass(User user) {

		        Connection connection = null;
		        try {
		            connection = getConnection();

		            String encPassword = CipherUtil.encrypt(user.getPassword());
		            user.setPassword(encPassword);

		            UserDao userDao = new UserDao();
		            userDao.updateNoPass(connection, user);

		            commit(connection);
		        } catch (RuntimeException e) {
		            rollback(connection);
		            throw e;
		        } catch (Error e) {
		            rollback(connection);
		            throw e;
		        } finally {
		            close(connection);
		        }
		    }
//同じく役職もリストへ
	public List<Department> getDepartments() {
		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			return userDao.getDepartments(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//userdaoのログイン情報からidとるためのやつ
	public User getUser(int userId) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			User user = userDao.getUser(connection, userId);

			commit(connection);

			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//停止か再開かを制御する
	public User isStopped(User user) {

		Connection connection = null;
		try {
			connection = getConnection();
			UserDao.isStopped(connection, user);

			commit(connection);

			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
