package bbs.dao;

import static bbs.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import bbs.beans.UserPost;
import bbs.exception.NoRowsUpdatedRuntimeException;
import bbs.exception.SQLRuntimeException;

public class UserPostDao {

	public List<UserPost> getUserPosts(Connection connection, int num) {
		//投稿に名前表示させるために表結合する 条件なしの一覧表示
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("posts.id as id, ");
			sql.append("posts.user_id as user_id, ");
			sql.append("posts.title as title, ");
			sql.append("posts.body as body, ");
			sql.append("posts.category as category, ");
			sql.append("posts.created_date as created_date, ");
			sql.append("posts.deleted as deleted, ");
			sql.append("users.name as name ");
			sql.append("FROM posts ");
			sql.append("INNER JOIN users ");
			sql.append("ON posts.user_id = users.id ");
			sql.append("ORDER BY created_date ASC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserPost> ret = toUserPostList(rs);
			//したのtouserを（sql文で実行した結果が入っているrsを引数に）
			//実行し、取得した行などの情報を実際に変数などに入れて見れる値としてまとめ、setで新しいUserPostビーンズ変数postをつかってまとめる
			//そして、まとめた全ての情報をリストretにaddしそのけっかをリストに格納する
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//カテゴリー検索した後の該当投稿一覧表示 一気にするようにまとめたから使ってない
	public List<UserPost> Category(Connection connection, int num, String category) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("posts.id as id, ");
			sql.append("posts.user_id as user_id, ");
			sql.append("posts.title as title, ");
			sql.append("posts.body as body, ");
			sql.append("posts.category as category, ");
			sql.append("posts.created_date as created_date, ");
			sql.append("posts.deleted as deleted, ");
			sql.append("users.name as name ");
			sql.append("FROM posts ");
			sql.append("INNER JOIN users ");
			sql.append("ON posts.user_id = users.id ");
			sql.append("WHERE category LIKE ? ");
			sql.append("ORDER BY created_date ASC limit " + num);

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, "%" + category + "%");

			ResultSet rs = ps.executeQuery();
			List<UserPost> ret = toUserPostList(rs);
			return ret;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserPost> toUserPostList(ResultSet rs)
			throws SQLException {

		List<UserPost> ret = new ArrayList<UserPost>();
		try {
			while (rs.next()) {
				//読みたいrsのデータを用意した変数に入れていく
				String title = rs.getString("title");
				String body = rs.getString("body");
				String category = rs.getString("category");
				Timestamp createdDate = rs.getTimestamp("created_date");
				String name = rs.getString("name");
				int id = rs.getInt("id");
				int userId = rs.getInt("user_id");
				int delete = rs.getInt("deleted");

				//rsから読み取り変数に入れたので、それを使ってデータをusrepostビーンズにまとめる
				UserPost post = new UserPost();
				post.setTitle(title);
				post.setBody(body);
				post.setCategory(category);
				post.setName(name);
				post.setCreated_date(createdDate);
				post.setId(id);
				post.setUser_id(userId);
				post.setDelete(delete);

				ret.add(post);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	//カテゴリーを全部持ってくるためのもの 仕様はタブ選択ではなく文字入力だからいらなかった
	public List<UserPost> getCategoryAll(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT DISTINCT category FROM posts");//被りなし

			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			List<UserPost> ret = toCategoryList(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//カテゴリのrsを読む 使ってない
	private List<UserPost> toCategoryList(ResultSet rs)
			throws SQLException {

		List<UserPost> ret = new ArrayList<UserPost>();
		try {
			while (rs.next()) {
				String category = rs.getString("category");
				UserPost message = new UserPost();
				message.setCategory(category);
				ret.add(message);
			}
			return ret;
		} finally {
			close(rs);
		}

	}

	//最古の投稿日時を取得する
	public static String getFirstDate(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT MIN(created_date) AS created_date FROM posts");

			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			String ret = toStringDate(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	// 最新の投稿日時を取得する

	public static String getLastDate(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT MAX(created_date) AS created_date FROM posts");

			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			String ret = toStringDate(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//日時ｓｑｌ結果ｒｓを読み込んで返す
	private static String toStringDate(ResultSet rs)
			throws SQLException {
		SimpleDateFormat sdf = new SimpleDateFormat();
		sdf.applyPattern("yyyy-MM-dd");
		String ret = new String();

		try {
			while (rs.next()) {
				Timestamp createdAt = rs.getTimestamp("created_date");
				ret = sdf.format(createdAt);

			}
			return ret;
		} finally {
			close(rs);
		}
	}

	//日時とカテゴリの検索条件をうけて投稿の一覧表示
	public List<UserPost> searchPost(Connection connection, String category,
			String firstDate, String lastDate,int sort) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("posts.id as id, ");
			sql.append("posts.user_id as user_id, ");
			sql.append("posts.title as title, ");
			sql.append("posts.body as body, ");
			sql.append("posts.category as category, ");
			sql.append("posts.created_date as created_date, ");
			sql.append("posts.deleted as deleted, ");
			sql.append("users.name as name ");
			sql.append("FROM posts ");
			sql.append("INNER JOIN users ");
			sql.append("ON posts.user_id = users.id ");
			sql.append("WHERE posts.created_date BETWEEN ? AND ?");

			SimpleDateFormat sdf = new SimpleDateFormat();
			sdf.applyPattern("yyyy-MM-dd HH:mm:ss");
			if (!(StringUtils.isEmpty(category))) {
				sql.append(" AND category LIKE ?");
				if (sort == 0){
					sql.append("ORDER BY created_date ASC ");
				} else {
					sql.append("ORDER BY created_date DESC ");
				}

				ps = connection.prepareStatement(sql.toString());
				ps.setString(1, firstDate);
				ps.setString(2, lastDate);
				ps.setString(3, "%" + category + "%");

			} else {
				if (sort == 0){
					sql.append("ORDER BY created_date ASC ");
				} else {
					sql.append("ORDER BY created_date DESC ");
				}

				ps = connection.prepareStatement(sql.toString());
				ps.setString(1, firstDate);
				ps.setString(2, lastDate);
			}

			ResultSet rs = ps.executeQuery();
			List<UserPost> ret = toPostList(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	// 絞り込み検索SQL実行結果から投稿のリストを取得

	private  List<UserPost> toPostList(ResultSet rs) throws SQLException {

		List<UserPost> ret = new ArrayList<UserPost>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String title = rs.getString("title");
				String body = rs.getString("body");
				String category = rs.getString("category");
				Timestamp createdAt = rs.getTimestamp("created_date");
				int userId = rs.getInt("user_id");
				String name = rs.getString("name");
				int delete = rs.getInt("deleted");

				UserPost post = new UserPost();
				//UserPostbeansのpostという口座のidにセットするということ
				post.setId(id);
				post.setBody(body);
				post.setTitle(title);
				post.setCategory(category);
				post.setCreated_date(createdAt);
				post.setUser_id(userId);
				post.setName(name);
				post.setDelete(delete);

				ret.add(post);
			}

			return ret;
		} finally {
			close(rs);
		}
	}

	//投稿削除dao 物理削除ではなく、deleteカラムの値で0が表示 1が非表示としたい
	public static void delete(Connection connection, UserPost deletepost) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE posts SET");
			sql.append(" deleted = ?");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, deletepost.getDelete());
			ps.setInt(2, deletepost.getId());

			int count = ps.executeUpdate();

			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}
