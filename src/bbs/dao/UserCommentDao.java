package bbs.dao;

import static bbs.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import bbs.beans.UserComment;
import bbs.exception.SQLRuntimeException;

public class UserCommentDao {

	public List<UserComment> getUserComments(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("comments.id as id, ");
			sql.append("comments.deleted as deleted, ");
			sql.append("comments.post_id as post_id, ");
			sql.append("comments.user_id as user_id, ");
			sql.append("comments.comment as comment, ");
			sql.append("comments.created_date as created_date, ");
			sql.append("comments.updated_date as updated_date, ");
			sql.append("users.name as name ");
			sql.append("FROM comments ");
			sql.append("INNER JOIN users ");
			sql.append("ON comments.user_id = users.id ");
			sql.append("ORDER BY created_date ASC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserComment> ret = toUserCommentList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserComment> toUserCommentList(ResultSet rs)
			throws SQLException {

		List<UserComment> ret = new ArrayList<UserComment>();
		try {
			while (rs.next()) {
				int Id = rs.getInt("id");
				String comment1 = rs.getString("comment");
				Timestamp createdDate = rs.getTimestamp("created_date");
				Timestamp updatedDate = rs.getTimestamp("updated_date");
				String name = rs.getString("name");
				int PostId = rs.getInt("post_id");
				int UserId = rs.getInt("user_id");
				int Delete = rs.getInt("deleted");

			    UserComment comment = new UserComment();

			    comment.setId(Id);
				comment.setComment(comment1);
				comment.setCreated_date(createdDate);
				comment.setUpdated_date(updatedDate);
				comment.setName(name);
				comment.setPost_id(PostId);
				comment.setUser_id(UserId);//コメントのユーザid
				comment.setDelete(Delete);
;
				ret.add(comment);
			}
			return ret;
		} finally {
			close(rs);
		}
	}


}