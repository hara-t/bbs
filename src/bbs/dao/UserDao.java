package bbs.dao;

import static bbs.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import bbs.beans.Branch;
import bbs.beans.Department;
import bbs.beans.User;
import bbs.exception.NoRowsUpdatedRuntimeException;
//import bbs.exception.NoRowsUpdatedRuntimeException;
import bbs.exception.SQLRuntimeException;

//import org.apache.commons.lang3.StringUtils;
public class UserDao {

	public void insert(Connection connection, User user) {
//ユーザー新規登録のための処理
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("login_id");
			sql.append(", name");
			sql.append(", password");
			//            sql.append(", stop_ornot");
			sql.append(", branch");
			sql.append(", department");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append("?"); // login_id
			sql.append(", ?"); // name
			sql.append(", ?"); // password
			/// sql.append(", ?"); // stop_ornot
			sql.append(", ?"); // branch
			sql.append(", ?"); // department
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(", CURRENT_TIMESTAMP"); // updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLoginId());
			ps.setString(2, user.getName());
			ps.setString(3, user.getPassword());
			ps.setInt(4, user.getBranch());
			ps.setInt(5, user.getDepartment());
			// ps.setInt(4 , user.getIsStopped());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	//branchdao
	public List<Branch> getBranches(Connection conn) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM branches";
			ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			return toBranchList(rs);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Branch> toBranchList(ResultSet rs) throws SQLException {
		List<Branch> ret = new ArrayList<>();

		while (rs.next()) {
			int id = rs.getInt("branch");
			String name = rs.getString("branch_name");

			Branch branch = new Branch();
			branch.setId(id);
			branch.setName(name);

			ret.add(branch);
		}

		return ret;
	}
	//ここまで支店

	//役職dao
	public List<Department> getDepartments(Connection conn) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM departments";
			ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			return toDepartmentList(rs);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Department> toDepartmentList(ResultSet rs) throws SQLException {
		List<Department> ret = new ArrayList<>();

		while (rs.next()) {
			int id = rs.getInt("department");
			String name = rs.getString("department_name");

			Department department = new Department();
			department.setId(id);
			department.setName(name);

			ret.add(department);
		}

		return ret;
	}

	//役職ここまで

	//ログイン処理　ログインidとパスワード
	public User getUser(Connection connection, String loginId,
			String password) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE login_id = ? AND password = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, loginId);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//ＤＢから読み取ったrsを読み取って格納する
	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				/*行からデータを取得*/
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				String password = rs.getString("password");
				int isStopped = rs.getInt("stop_ornot");
				int branch = rs.getInt("branch");
				int department = rs.getInt("department");
				Timestamp createdDate = rs.getTimestamp("created_date");
				Timestamp updatedDate = rs.getTimestamp("updated_date");


				User user = new User();
				user.setId(id);
				user.setLoginId(loginId);
				user.setName(name);
				user.setPassword(password);
				user.setIsStopped(isStopped);
				user.setBranch(branch);
				user.setDepartment(department);
				user.setCreatedDate(createdDate);
				user.setUpdatedDate(updatedDate);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}


	//ログインidからユーザーズ情報をとるためのgetUser　引数が違うから同じ名前でも大丈夫　型で自動でどっちか判断する
	public User getUser(Connection connection, int id) {

	    PreparedStatement ps = null;
	    try {
	        String sql = "SELECT * FROM users WHERE id = ?";

	        ps = connection.prepareStatement(sql);
	        ps.setInt(1, id); //?にidを入れる

	        ResultSet rs = ps.executeQuery();
	        List<User> userList = toUserList(rs);
	        if (userList.isEmpty() == true) {
	            return null;
	        } else if (2 <= userList.size()) {
	            throw new IllegalStateException("2 <= userList.size()");
	        } else {
	            return userList.get(0);
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}

//管理権限を持つ人によるユーザー情報の編集

	 public void update(Connection connection, User user) {

	        PreparedStatement ps = null;
	        try {
	            StringBuilder sql = new StringBuilder();
	            sql.append("UPDATE users SET");
	            sql.append(" login_id = ?");
	            sql.append(", name = ?");
	            sql.append(", password = ?");
	            sql.append(", branch = ?");
	            sql.append(", department = ?");
	            sql.append(", updated_date = CURRENT_TIMESTAMP");
	            sql.append(" WHERE");
	            sql.append(" id = ?");

	            ps = connection.prepareStatement(sql.toString());

	            ps.setString(1, user.getLoginId());
	            ps.setString(2, user.getName());
	            ps.setString(3, user.getPassword());
	            ps.setInt(4, user.getBranch());
	            ps.setInt(5, user.getDepartment());
	            ps.setInt(6, user.getId());

	            int count = ps.executeUpdate();
	            if (count == 0) {
	                throw new NoRowsUpdatedRuntimeException();
	            }
	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps);
	        }

	    }
	//管理権限を持つ人によるユーザー情報の編集パスワードが無いとき

		 public void updateNoPass(Connection connection, User user) {

		        PreparedStatement ps = null;
		        try {
		            StringBuilder sql = new StringBuilder();
		            sql.append("UPDATE users SET");
		            sql.append(" login_id = ?");
		            sql.append(", name = ?");
		            sql.append(", branch = ?");
		            sql.append(", department = ?");
		            sql.append(", updated_date = CURRENT_TIMESTAMP");
		            sql.append(" WHERE");
		            sql.append(" id = ?");

		            ps = connection.prepareStatement(sql.toString());

		            ps.setString(1, user.getLoginId());
		            ps.setString(2, user.getName());
		            ps.setInt(3, user.getBranch());
		            ps.setInt(4, user.getDepartment());
		            ps.setInt(5, user.getId());

		            int count = ps.executeUpdate();
		            if (count == 0) {
		                throw new NoRowsUpdatedRuntimeException();
		            }
		        } catch (SQLException e) {
		            throw new SQLRuntimeException(e);
		        } finally {
		            close(ps);
		        }

		    }
	//ユーザー管理画面に一覧を表示するための処理　名前　支店名　役職名をセットに
	//ユーザー編集画面で情報を表示するためにログインidとかもいれておく
	public List<User> getUsers(Connection conn) {
		PreparedStatement ps = null;
		try {
		            StringBuilder sql = new StringBuilder();
		            sql.append("SELECT ");
		            sql.append("users.id as id, ");
		            sql.append("users.name as name, ");
		            sql.append("users.login_id as login_id, ");
		            sql.append("users.password as password, " );
		            sql.append("branches.branch_name as branch_name, ");
		            sql.append("departments.department_name as department_name, ");
		            sql.append("users.stop_ornot as stop_ornot ");
		            /*sql.append("users.branch,  ");
		            sql.append("users.department, ");
		            sql.append("departments.department, ");
		            sql.append("branches.branch as branch, ");
		            sql.append("departments.department as department, ");*/
		            sql.append("FROM users ");
		            sql.append("INNER JOIN branches ");
		            sql.append("ON users.branch = branches.branch ");
		            sql.append("INNER JOIN departments ");
		            sql.append("ON users.department = departments.department ");
		            sql.append("ORDER BY id DESC  " );

		            ps = conn.prepareStatement(sql.toString());

		            ResultSet rs = ps.executeQuery();
		            List<User> ret = togetUsers(rs);
			return  ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
//これは何回も使える　 rsの処理だから何回でも使える　なるべく再利用
	private List<User> togetUsers(ResultSet rs) throws SQLException {
		List<User> ret = new ArrayList<>();

		while (rs.next()) {
			int id = rs.getInt("id");
			String name = rs.getString("name");
			String branchname = rs.getString("branch_name");
			String departmentname = rs.getString("department_name");
			int stop_ornot = rs.getInt("stop_ornot");

			User user = new User();
			user.setIsStopped(stop_ornot);
			user.setId(id);
			user.setName(name);
			user.setBranch_name(branchname);
			user.setDepartment_name(departmentname);

			ret.add(user);
		}

		return ret;
	}

	public static void isStopped(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" stop_ornot = ?");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, user.getIsStopped());
			ps.setInt(2, user.getId());

			int count = ps.executeUpdate();

			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

}