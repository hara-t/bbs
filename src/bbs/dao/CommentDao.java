package bbs.dao;

import static bbs.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import bbs.beans.Comment;
import bbs.beans.UserPost;
import bbs.exception.NoRowsUpdatedRuntimeException;
import bbs.exception.SQLRuntimeException;

public class CommentDao {

	public void insert(Connection connection, Comment comment) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO comments ( ");
			sql.append("comment");
			sql.append(", post_id");
			sql.append(", user_id");
			sql.append(", branch");
			sql.append(", department");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append(" ?"); //
			sql.append(", ?");//
			sql.append(", ?");//
			sql.append(", ?");//
			sql.append(", ?"); //
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(", CURRENT_TIMESTAMP"); // updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, comment.getComment());
			ps.setInt(2, comment.getPost_id());
			ps.setInt(3, comment.getUser_id());
			ps.setInt(4, comment.getBranch());
			ps.setInt(5, comment.getDepartment());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//コメント削除dao 物理削除ではなく、deleteカラムの値で0が表示 1が非表示としたい
	public static void delete(Connection connection, Comment comment) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE comments SET");
			sql.append(" deleted = ?");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, comment.getDelete());
			ps.setInt(2, comment.getId());
			//何これ？stopのアップデートにあったからとりあえず入れる
			int count = ps.executeUpdate();

			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//特定の投稿に対するコメントすべてを消す
	public static void deletePostComment(Connection connection, UserPost deletepostcomment) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE comments SET"); //指定のpostidへのコメントのdeletedカラムを１に
			sql.append(" deleted = ?");
			sql.append(" WHERE");
			sql.append(" post_id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, deletepostcomment.getDelete());//deletepostサーブでセットした変数からdeleteの値を読み込む
			ps.setInt(2, deletepostcomment.getId()); //持ってきた変数からpostidの値を読み込む

			int count = ps.executeUpdate();

			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}
