package bbs.dao;

import static bbs.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import bbs.beans.User;
import bbs.exception.SQLRuntimeException;

@WebServlet("/SearchDao")
public class SearchDao extends HttpServlet {


	public List<User> Search(Connection conn) {
		PreparedStatement ps = null;
		try {
		            StringBuilder sql = new StringBuilder();
		            sql.append("SELECT ");
		            sql.append("users.id as id, ");
		            sql.append("users.name as name, ");
		            sql.append("users.login_id as login_id, ");
		            sql.append("users.password as password, " );
		            sql.append("branches.branch_name as branch_name, ");
		            sql.append("departments.department_name as department_name, ");
		            sql.append("users.stop_ornot as stop_ornot ");
		            sql.append("FROM users ");
		            sql.append("INNER JOIN branches ");
		            sql.append("ON users.branch = branches.branch ");
		            sql.append("INNER JOIN departments ");
		            sql.append("ON users.department = departments.department ");
		            sql.append("ORDER BY id DESC  " );

		            ps = conn.prepareStatement(sql.toString());

		            ResultSet rs = ps.executeQuery();
		            List<User> ret = togetUsers(rs);
			return  ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
//これは何回も使える　 rsの処理だから何回でも使える　なるべく再利用
	private List<User> togetUsers(ResultSet rs) throws SQLException {
		List<User> ret = new ArrayList<>();

		while (rs.next()) {
			int id = rs.getInt("id");
			String name = rs.getString("name");
			String branchname = rs.getString("branch_name");
			String departmentname = rs.getString("department_name");
			int stop_ornot = rs.getInt("stop_ornot");

			User user = new User();
			user.setIsStopped(stop_ornot);
			user.setId(id);
			user.setName(name);
			user.setBranch_name(branchname);
			user.setDepartment_name(departmentname);

			ret.add(user);
		}

		return ret;
	}
}
