package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bbs.beans.Comment;
import bbs.beans.User;
import bbs.service.CommentService;

@WebServlet(urlPatterns = { "/newComment" })
public class NewCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		request.getRequestDispatcher("home.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<String> comments = new ArrayList<String>();

		if (isValid(request, comments) == true) {

			User user = (User) session.getAttribute("loginUser");

			Comment comment = new Comment();
			comment.setComment(request.getParameter("comment"));
			comment.setPost_id(Integer.parseInt(request.getParameter("autoinsert")));
			comment.setUser_id(user.getId());

			new CommentService().register(comment);

			response.sendRedirect("./");
		} else {
			session.setAttribute("errorMessages", comments);
			response.sendRedirect("./");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> error) {
		//ここはエラーメッセージの設定　not empty error message のところ"errormesages" message とあったやつ
		//messageをerrorに変えたからあとであわせないとtopのjspエラメのとこね
		String comment = request.getParameter("comment");

		if (StringUtils.isEmpty(comment) == true) {
			error.add("コメントを入力してください");
		}
		if (500 < comment.length()) {
			error.add("コメントは500文字以下で入力してください");
		}
		if (error.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}