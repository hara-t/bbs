package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bbs.beans.Message;
import bbs.beans.User;
import bbs.beans.UserPost;
import bbs.service.MessageService;

@WebServlet(urlPatterns = { "/newMessage" })
public class NewMessageServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("post.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession(); //セッションの開始

        List<String> messages = new ArrayList<String>();

        if (isValid(request, messages) == true) {

            User user = (User) session.getAttribute("loginUser");
            Message message = new Message();
            message.setCategory(request.getParameter("category"));
            message.setTitle(request.getParameter("title"));
            message.setBody(request.getParameter("body"));
            message.setUserId(user.getId());//User クラスで作ったgetIdでユーザーidをいれたい

            new MessageService().register(message);
            session.removeAttribute("postinput");

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("newMessage");
            //request.getRequestDispatcher("newMessage").forward(request,response);
            //forwardさせて入力値も表示　したいけど
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

        String category = request.getParameter("category");
        String title = request.getParameter("title");
        String body = request.getParameter("body");

        //入力値がエラーでても消えないようにとりあえず置いておく
        UserPost post = new UserPost();
        post.setCategory(category);
        post.setTitle(title);
        post.setBody(body);
        HttpSession session = request.getSession();
        session.setAttribute("postinput", post);

        if (StringUtils.isEmpty(category) == true) {
            messages.add("カテゴリーを入力してください");
        }
        if (10 < category.length()) {
            messages.add("カテゴリーは10文字以下で入力してください");
        }
        if (StringUtils.isEmpty(title) == true) {
            messages.add("件名を入力してください");
        }
        if (30 < title.length()) {
            messages.add("件名は30文字以下で入力してください");
        }
        if (StringUtils.isEmpty(body) == true) {
            messages.add("本文を入力してください");
        }
        if (1000 < body.length()) {
            messages.add("本文は1000文字以下で入力してください");
        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}
