package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bbs.beans.User;
import bbs.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	UserService service = new UserService();
    	request.setAttribute("branches", service.getBranches());
        request.setAttribute("departments",service.getDepartments());
        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<String>();

        HttpSession session = request.getSession();
        if (isValid(request, messages) == true) {

//エラーメッセージがひとつでもあれば登録せずにsignup画面にもどす
            User user = new User();
            user.setName(request.getParameter("name"));
            user.setLoginId(request.getParameter("login_id"));
            user.setPassword(request.getParameter("password"));
            user.setBranch(Integer.parseInt(request.getParameter("branch")));
            user.setDepartment(Integer.parseInt(request.getParameter("department")));
            //user.setIsStopped(Integer.parseInt(request.getParameter("stop_ornot")));
           //デフォルト0にしたからそもそもいらない↑

 //ここまでだとuserにセットしただけだから、下の一文でDBにも入れる必要あり
            new UserService().register(user);

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("signup");
        }
    }

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String loginid = request.getParameter("login_id");
		String password = request.getParameter("password");
		String passcheck =request.getParameter("passcheck");
		String name = request.getParameter("name");

		if (StringUtils.isEmpty(name) == true) {
			messages.add("名前を入力してください");
		}
		if (name.length() >= 11) {
			messages.add("名前は10文字以内で入力してください");
		}
		if (StringUtils.isEmpty(loginid) == true) {
			messages.add("ログインidを入力してください");
		}
		if (loginid.length() <  6|| loginid.length() > 20) {
			messages.add("ログインidは6文字以上20文字以下で入力してください");
		}
		if (!loginid.matches("[A-Za-z0-9]+")) {
			messages.add("ログインidは半角英数字で入力してください");
		}
		if (StringUtils.isEmpty(password) == true) {
			messages.add("パスワードを入力してください");
		}
		if (!(password.matches(".*[^\\u0020-\\u007E].*")== false)) {
			messages.add("パスワードは記号を含む全ての半角文字で入力してください");
		}
		if(!password.equals(passcheck)) {
			messages.add("パスワードは同じものを二度入力してください");
		}
		if (password.length() < 6 || password.length() > 20) {
			messages.add("パスワードは6文字以上20文字以下で入力してください");
		}


		// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}
