package bbs.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bbs.beans.UserPost;
import bbs.service.CommentService;
import bbs.service.MessageService;


@WebServlet(urlPatterns = { "/deletePost" })
public class DeletePostServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

        protected void doPost(HttpServletRequest request, HttpServletResponse response)
				throws IOException, ServletException {

        	UserPost deletepost = new UserPost();

        	deletepost.setDelete(Integer.parseInt(request.getParameter("deleted")));
        	deletepost.setId(Integer.parseInt(request.getParameter("autopostid")));
			new MessageService().deletePost(deletepost);
			new CommentService().deletePostComment(deletepost);

			request.setAttribute("post", deletepost);

        response.sendRedirect("./");
    }
}


