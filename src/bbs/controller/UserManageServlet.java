package bbs.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bbs.beans.User;
import bbs.service.UserService;

@WebServlet(urlPatterns = { "/userManage" })
public class UserManageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		List<User> users = new UserService().getUsers();

		request.setAttribute("users", users);

		request.getRequestDispatcher("usermanage.jsp").forward(request, response);
	}

			protected void doPost(HttpServletRequest request, HttpServletResponse response)
					throws IOException, ServletException {
// 画面で入力されたstopornotと、hiddenでとってきた情報をusersにいれておく　とったidのところに、stopornotをいれたいから
				User users = new User();

				users.setIsStopped(Integer.parseInt(request.getParameter("stop_ornot")));
				users.setId(Integer.parseInt(request.getParameter("id")));
				new UserService().isStopped(users);
				request.setAttribute("users", users);
				response.sendRedirect("userManage");
	}
}


