package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bbs.beans.Comment;
import bbs.service.CommentService;

@WebServlet(urlPatterns = { "/deleteComment" })
public class DeleteCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();

		Comment comment = new Comment();

		comment.setDelete(Integer.parseInt(request.getParameter("deleted")));
		comment.setId(Integer.parseInt(request.getParameter("autocommentid")));
		new CommentService().deleteComment(comment);
        messages.add("コメントを削除しました");
        //なぜセッションなのかわからん　requestにするとずっと表示されてしまう
        session.setAttribute("errorMessages", messages);
		request.setAttribute("comment", comment);

		response.sendRedirect("./");
		//session.removeAttribute("errorMessages");
	}
}

