package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bbs.beans.Branch;
import bbs.beans.Department;
import bbs.beans.User;
import bbs.exception.NoRowsUpdatedRuntimeException;
import bbs.service.UserService;

@WebServlet(urlPatterns = { "/userEdit" })
public class UserEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		//arefのid＝？の人のユーザー情報を取ってくるってこと？
		User editUser = new UserService().getUser(Integer.parseInt(request.getParameter("id")));
		List<Branch> branchList = new UserService().getBranches();
		List<Department> departmentList = new UserService().getDepartments();
		request.setAttribute("editUser", editUser);
		request.setAttribute("branches", branchList);
		request.setAttribute("departments", departmentList);
		request.getRequestDispatcher("useredit.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		//editUserには編集後の情報が入っている
		User editUser = getEditUser(request);

		if (isValid(request, messages) == true) {
			try {
				if (StringUtils.isEmpty(editUser.getPassword()) == false) {
					new UserService().update(editUser);

				} else if (StringUtils.isEmpty(editUser.getPassword()) == true) {
					new UserService().updateNoPass(editUser);
				}
			} catch (NoRowsUpdatedRuntimeException e) {
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				session.setAttribute("errorMessages", messages);
				request.setAttribute("editUser", editUser);
				//response.sendRedirect("userEdit");
				request.getRequestDispatcher("useredit.jsp").forward(request, response);
				return;
			}

			response.sendRedirect("userManage");
		} else {
			session.setAttribute("errorMessages", messages);
			request.setAttribute("editUser", editUser);
			request.getRequestDispatcher("useredit.jsp").forward(request, response);

		}
	}

	private User getEditUser(HttpServletRequest request)
			throws IOException, ServletException {

		//編集画面に表示するためにリストをとってきて名前付け
		List<Branch> branchList = new UserService().getBranches();
		List<Department> departmentList = new UserService().getDepartments();
		request.setAttribute("branches", branchList);
		request.setAttribute("departments", departmentList);

		User editUser = new User();
		//選択したユーザーのidをhiddenで飛ばしているので、そのidを持つユーザー情報を取得する
		editUser.setId(Integer.parseInt(request.getParameter("id")));
		//入力値をとってきてセット
		editUser.setName(request.getParameter("name"));
		editUser.setLoginId(request.getParameter("login_id"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setBranch(Integer.parseInt(request.getParameter("branch")));
		editUser.setDepartment(Integer.parseInt(request.getParameter("department")));
		return editUser;
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String loginid = request.getParameter("login_id");
		String password = request.getParameter("password");
		String passcheck = request.getParameter("passcheck");
		String name = request.getParameter("name");
		//		System.out.println(request.getParameter("login_id"));

		if (StringUtils.isEmpty(loginid) == true) {
			messages.add("ログインidを入力してください");
		}
		if (loginid.length() < 6 || loginid.length() > 20) {
			messages.add("ログインidは6文字以上20文字以下で入力してください");
		}
		if (!loginid.matches("[A-Za-z0-9]+")) {
			messages.add("ログインidは半角英数字で入力してください");
		}
		if (!(password.matches(".*[^\\u0020-\\u007E].*") == false)) {
			messages.add("パスワードは記号を含む全ての半角文字で入力してください");
		}
		if (StringUtils.isEmpty(password) != true && (password.length() < 6 || password.length() > 20)) {
			messages.add("パスワードは6文字以上20文字以下で入力してください");
		}
		if (StringUtils.isEmpty(password) == true) {

		} else if (!password.equals(passcheck)) {
			messages.add("パスワードは同じものを二度入力してください");
		}
		if (StringUtils.isEmpty(name) == true) {
			messages.add("名前を入力してください");
		}
		if (name.length() > 10) {
			messages.add("名前は10文字以内で入力してください");
		}

		// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
