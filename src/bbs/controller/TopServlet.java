package bbs.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bbs.beans.UserComment;
import bbs.beans.UserPost;
import bbs.service.CommentService;
import bbs.service.MessageService;

//ログインしてる人のuser情報をjspに送ってあげたい
@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<UserComment> comments = new CommentService().getComment();
		List<UserPost> posts = new MessageService().getPost();
		request.setAttribute("posts", posts);

		String category = request.getParameter("search");
		String firstDate = request.getParameter("firstDate");
		String lastDate = request.getParameter("lastDate");

		// ソート昇順降順 三項演算子 nullなら1入れるよ デフォルトはdescにする
		int sort = (request.getParameter("sort") == null) ? 1
		: Integer.parseInt(request.getParameter("sort"));


		// 開始日と終了日を比較 どちらが大きいか
		int compareDate = 0;
		if (firstDate != null && lastDate != null) {
			if (!firstDate.isEmpty() && !lastDate.isEmpty()) {
				compareDate = lastDate.compareTo(firstDate);
			} //comparetoは比較 変数１＜２なら負 １＞２なら正 ＝なら０となる
		}

		//開始日の入力がなければ、最初の投稿日を 入力があれば、日付けが早い方をfirstdateとする
		//時刻の前は半角空白が要る
String first = setFirst.setFirst(firstDate,lastDate,compareDate);

//				if(firstDate == null || firstDate.isEmpty() ) {
//					 first = new MessageService().getFirstDate();
//				}
//				if(compareDate < 0) {
//					 first = lastDate + " 00:00:00" ;
//				}else {
//					 first = firstDate + " 00:00:00" ;
//				}
String last = setLast.setLast(firstDate,lastDate,compareDate);

//１、とりあえず宣言しといて後から上書きするパターン　好ましくないみたい
//              String last= null;
//
//				if(lastDate == null || lastDate.isEmpty() ) {
//					last = new MessageService().getFirstDate();
//				}
//				if(compareDate < 0) {
//					 last = firstDate + " 23:59:59" ;
//				}else {
//					 last = lastDate + " 23:59:59" ;
//				}

		//２、三項演算子を使う場合 ぱっと見で分かりにくいのでメソッドに飛ばすようにした
//				String first = (firstDate == null) ? (new MessageService().getFirstDate())
//						: (firstDate.isEmpty()) ? (new MessageService().getFirstDate())
//								: (compareDate < 0) ? (lastDate + " 00:00:00")
//										: (firstDate + " 00:00:00");
//
//				String last = (lastDate == null) ? new MessageService().getLastDate()
//						: (lastDate.isEmpty()) ? new MessageService().getLastDate()
//								: (compareDate < 0) ? (firstDate + " 23:59:59")
//										: (lastDate + " 23:59:59");

		// 投稿の検索
		List<UserPost> searchPost = new MessageService().searchPost(category, first, last,sort);
		// 選択可能な日付を取得
		String minDate = new MessageService().getFirstDate();
		String maxDate = new MessageService().getLastDate();

		// 画面を表示するための情報の設定
		request.setAttribute("minDate", minDate);
		request.setAttribute("maxDate", maxDate); //そもそも表示のカレンダーで選択し絞りたい
		request.setAttribute("category", category);
		request.setAttribute("posts", searchPost);
		request.setAttribute("comments", comments);
		request.setAttribute("sort", sort);
		request.setAttribute("firstDate", firstDate);
		request.setAttribute("lastDate", lastDate);

		//sql文ひとつにまとめず、分岐させようとしてた名残
		//			if (StringUtils.isEmpty(category)) {
		//				System.out.println("a"); どちらに分岐したか調べる
		//				List<UserPost> posts = new MessageService().getPost();
		//				request.setAttribute("posts", posts);
		//
		//			} else {
		//				System.out.println("b");
		//				List<UserPost> categorysearch = new MessageService().Category(category);
		//				request.setAttribute("posts", categorysearch);
		//			}

		request.getRequestDispatcher("/top.jsp").forward(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		response.sendRedirect("./");
	}
}